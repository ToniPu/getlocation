package org.tonipu.getcoordinates;

import android.Manifest;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import android.os.Build.VERSION;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    Double Latitud,Longitud;
    TextView txtlat;
    TextView txtlon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        txtlat = findViewById(R.id.txtlat);
        txtlon = findViewById(R.id.txtlong);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtlat.setText("calculando coordenadas");
                txtlon.setText("Espera");
                get_location();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void get_location() {
        if (VERSION.SDK_INT < 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this,"Primero debes habilitar el gps",Toast.LENGTH_LONG);
                return;

            }
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    Latitud = Double.valueOf(location.getLatitude());
                    Longitud = Double.valueOf(location.getLongitude());
                    txtlat.setText(Latitud.toString());
                    txtlon.setText(Longitud.toString());
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                    mostrarAvisoGpsDeshabilitado();
                }
            };
            if (ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") ==PackageManager.PERMISSION_GRANTED&& ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates("gps", 0, 0.0f, locationListener);
            }
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            Log.d("Log","Pulsamos el botón de localización");
            LocationManager locationManager2 = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            LocationListener locationListener2 = new LocationListener() {
                public void onLocationChanged(Location location) {
                    Latitud = Double.valueOf(location.getLatitude());
                    Longitud = Double.valueOf(location.getLongitude());
                    txtlat.setText(Latitud.toString());
                    txtlon.setText(Longitud.toString());
                    return;
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                    mostrarAvisoGpsDeshabilitado();
                }
            };
            if (ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0 && ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
                locationManager2.requestLocationUpdates("gps", 0, 0.0f, locationListener2);
            }
        } else {
            Log.d("sos", "pedimos permiso gps");
            permisoCoarse();
            permisoFino();
            permisoInternet();

        }
    }

    public void mostrarAvisoGpsDeshabilitado() {
        Builder dialogo = new AlertDialog.Builder(this);
        dialogo.setMessage("El GPS ESTÁ DESACTIVADO, ¿QUIERES ACTIVARLO?");
        dialogo.setCancelable(false);
        dialogo.setPositiveButton("SEE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                LanzarActivarGPS();

            }
        });
        dialogo.setNegativeButton("NOO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialogo.show();
    }

    private void permisoFino()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            // no tenemos permiso
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION)){

            }else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},0);
            }
        }else{
            // ya tenemos acceso
        }
    }
    private void permisoCoarse()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            // no tenemos permiso
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_COARSE_LOCATION)){

            }else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},0);
            }
        }else{
            // ya tenemos acceso
        }
    }
    private void permisoInternet()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED){
            // no tenemos permiso
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.INTERNET)){

            }else{
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.INTERNET},0);
            }
        }else{
            // ya tenemos acceso
        }
    }

    public void LanzarActivarGPS() {
        startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
    }

}
